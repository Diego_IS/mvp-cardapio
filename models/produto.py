from database import db, Column, Integer, String, Numeric

class Produto(db.Model):
    __tablename__ = "produto"

    id = Column(Integer, primary_key=True, autoincrement=True)
    nome = Column(String(45), nullable=False)
    preco = Column(Numeric, nullable=False)
    qtd_estoque = Column(Integer, nullable=False)

    def __init__(self, nome:str, preco:float, qtd_estoque:int):
        self.nome = nome
        self.preco = preco
        self.qtd_estoque = qtd_estoque
    
    def cadastrar(self):
        db.session.add(self)
        db.session.commit()
    
    @staticmethod
    def listar() -> list:
        lista_produtos = Produto.query.all()
        return lista_produtos

    def editar(self, novo_nome:str, novo_preco:float, novo_qtd_estoque:int):
        self.nome = novo_nome
        self.preco = novo_preco
        self.qtd_estoque = novo_qtd_estoque
        db.session.add(self)
        db.session.commit()
    
    def deletar(self):
        db.session.delete(self)
        db.session.commit()