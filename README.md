# mvp-cardapio
Este projeto trata-se de um sistema para realização e gerenciamento de pedidos em estabelecimentos do ramo alimentício.
Com ele os usuários poderão realizar pedidos e cancelar pedidos. E os estabelecimentos poderão aceitar ou recusar os pedidos.

Trata-se portanto de um mínimo produto viavel.