from flask import Blueprint, request, render_template
from flask_login import login_required

from models.produto import Produto

produto_bluepinrt = Blueprint("produto", __name__)

@produto_bluepinrt
@login_required
def cadastrar_produto():
    nome = request.form.get('nome')
    preco = request.form.get('preco')
    qtd_estoque = request.form.get('qtd-estoque')
    
    produto = Produto(nome, preco, qtd_estoque).cadastar()
    return render_template("")

@produto_bluepinrt
@login_required
def listar_produtos():
    lista_produtos = Produto.listar()
    return render_template("", lista_produtos)

@produto_bluepinrt
@login_required
def editar_produto():
    id_produto = request.form.get('id-produto')
    novo_nome = request.form.get('novo-nome')
    novo_preco = request.form.get('novo-preco')
    novo_qtd_estoque = request.form.get('novo-qtd-estoque')

    produto = Produto.query.get(id_produto)
    produto.editar(novo_nome, novo_preco, novo_qtd_estoque)
    return render_template("")

@produto_bluepinrt
@login_required
def deletar_produto():
    id_produto = request.form.get('id-produto')
    produto = Produto.query.get(id_produto)
    produto.deletar()
    return render_template("")