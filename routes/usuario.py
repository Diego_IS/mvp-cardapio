from flask import Blueprint, request, render_template
from flask_login import login_required, login_user, logout_user, current_user

from models.usuario import Usuario

usuario_blueprint = Blueprint('usuario', __name__)

@usuario_blueprint.route("/")
def index():
    return render_template("index.html")

@usuario_blueprint.route("/cadastrar", methods=["GET", "POST"])
def cadastrar_usuario():
    nome = request.form.get('nome')
    email = request.form.get('email')
    senha = request.form.get('senha')
    tipo_usuario = "cliente"

    usuario = Usuario(nome, email, senha, tipo_usuario)
    usuario.cadastrar()
    login_user(usuario)
    return render_template('painel.html', usuario=current_user)

@usuario_blueprint.route("/login", methods=["GET", "POST"])
def logar():
    usuario = Usuario.query.get(request.form.get('email'))
    login_user(usuario)
    return render_template('painel.html', usuario=current_user)

@usuario_blueprint.route("/logout")
def logout():
    logout_user()
    return render_template('index.html')

@usuario_blueprint.route("/deletar")
@login_required
def deletar_usuario():
    email_usuario = request.form.get("email")
    usuario = Usuario.query.get(email_usuario)
    usuario.deletar()
    return render_template("index.html")