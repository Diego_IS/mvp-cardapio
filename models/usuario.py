from database import db, Column, Integer, String, Enum
from flask_login import UserMixin

class Usuario(db.Model, UserMixin):
    __tablename__ = "usuario"

    nome = Column(String(45), nullable=False)
    email = Column(String(45), primary_key=True)
    senha = Column(String(64), nullable=False)
    tipo_usuario = Column(Enum('cliente', 'funcionario', 'adm'), nullable=False)

    def __init__(self, nome:str, email:str, senha:str, tipo_usuario:str):
        self.nome = nome
        self.email = email
        self.senha = senha
        self.tipo_usuario = tipo_usuario
    
    def cadastrar(self):
        db.session.add(self)
        db.session.commit()
    
    @staticmethod
    def listar() -> list:
        lista_usuarios = Usuario.query.all()
        return lista_usuarios
    
    def editar(self, novo_nome:str, novo_email:str, nova_senha:str, novo_tipo_usuario:str):
        self.nome = novo_nome
        self.email = novo_email
        self.senha = nova_senha
        self.tipo_usuario = novo_tipo_usuario
        db.session.add(self)
        db.session.commit()
    
    def deletar(self):
        db.session.delete(self)
        db.session.commit()
    
    def get_id(self):
        return self.email