from database import db, Column, Integer, String, Date, ForeignKey

class Pedido(db.Model):
    __tablename__ = "pedido"

    id = Column(Integer, primary_key=True, autoincrement=True)
    n_pedido = Column(String(4), nullable=False)
    data = Column(Date, nullable=False)
    usuario = Column(ForeignKey('usuario.id'), nullable=False)

    def __init__(self, n_pedido:str, data:Date, usuario:int):
        self.n_pedido = n_pedido
        self.data = data
        self.usuario = usuario

    @staticmethod
    def listar() -> list:
        lista_pedidos = Pedido.query.all()
        return lista_pedidos
    
    def editar(self, novo_n_pedido:str, nova_data:Date, novo_usuario:int):
        self.n_pedido = novo_n_pedido
        self.data = nova_data
        self.usuario = novo_usuario
        db.session.add(self)
        db.session.commit()
    
    def deletar(self):
        db.session.delete(self)
        db.session.commit()