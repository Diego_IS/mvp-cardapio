from database import db, Column, Integer, Numeric, ForeignKey

class PedidoHasProduto(db.Model):
    __tablename__ = "pedido_has_produto"

    pedido_id = Column(ForeignKey('pedido.id'))
    produto_id = Column(ForeignKey('produto.id'))
    qtd = Column(Integer, nullable=False)
    preco = Column(Numeric, nullable=False)

    def __init__(self, pedido_id:int, produto_id:int, qtd:int, preco:float):
        self.pedido_id = pedido_id
        self.produto_id = produto_id
        self.qtd = qtd
        self.preco = preco
    
    def cadastrar(self):
        db.session.add(self)
        db.session.commit()

    def editar(self, novo_produto_id:int, nova_qtd:int, novo_preco:float):
        self.produto_id = novo_produto_id
        self.qtd = nova_qtd
        self.preco = novo_preco
        db.session.add(self)
        db.session.commit()
    
    def deletar(self):
        db.session.delete(self)
        db.session.commit()