from flask import Flask, url_for
from secrets import token_hex
from flask_login import LoginManager
from flask_migrate import Migrate

#from routes.pedido import pedido_blueprint
#from routes.produto import produto_bluepinrt
from routes.usuario import usuario_blueprint

from database import db
from models.usuario import Usuario


login_manager = LoginManager()

app = Flask(__name__)
db.init_app(app)
login_manager.init_app(app)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///meubanco.db"
app.config["SECRET_KEY"] = token_hex()
migrate = Migrate(app, db)

#app.register_blueprint(pedido_blueprint, url_prefix="pedido")
#app.register_blueprint(produto_bluepinrt)
app.register_blueprint(usuario_blueprint)

@login_manager.user_loader
def carregar_usuario(id:int):
    return Usuario.query.get(id)

app.run("0.0.0.0", debug=True)