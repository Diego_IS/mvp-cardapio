from flask import Blueprint, request, render_template
from flask_login import login_required

from models.pedido import Pedido
from models.produto import Produto
from models.usuario import Usuario
from models.pedido_has_produto import PedidoHasProduto

pedido_blueprint = Blueprint("pedido", __name__)

@pedido_blueprint.route("/cadastrar")
@login_required
def cadastrar_pedido():
    cliente = request.form.get('id-cliente')
    data = request.form.get('data')

    for produto in request.form.get('produtos'):
        PedidoHasProduto()